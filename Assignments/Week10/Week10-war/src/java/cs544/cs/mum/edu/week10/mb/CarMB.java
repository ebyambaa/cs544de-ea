/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs544.cs.mum.edu.week10.mb;

import cs544.cs.mum.edu.week10.entity.Car;
import java.io.Serializable;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 *
 * @author Enkhee
 */
@Named
//@RequestScoped
@SessionScoped
public class CarMB implements Serializable {

    @PersistenceContext(unitName = "Week10-warPU")
    private EntityManager em;
    private Car car;

    public Car getCar() {
        return this.car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    /**
     * Creates a new instance of CarMB
     */
    public CarMB() {
    }

    public String create(){
        car = new Car();
        return "car";
    }
    
    @Transactional
    public String save() {
        car = em.merge(car);
//        em.persist(car);
        return "index";
    }

    @Transactional
    public String update(Car carIn) {
        car = em.find(Car.class, carIn.getId());
        return "car";
    }
}
