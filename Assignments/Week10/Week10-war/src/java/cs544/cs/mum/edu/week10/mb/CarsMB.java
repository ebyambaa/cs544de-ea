/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs544.cs.mum.edu.week10.mb;

import cs544.cs.mum.edu.week10.entity.Car;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 *
 * @author Enkhee
 */
@Named
@RequestScoped
public class CarsMB {

    @PersistenceContext(unitName = "Week10-warPU")
    private EntityManager em;
    private Car car;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }
    /**
     * Creates a new instance of CarsMB
     */
    public CarsMB() {
    }
    
    public List<Car> getCars(){
        return Car.getAllCars(em);
    }
    
    public int getCarsCount(){
        return Car.getAllCars(em).size();
    }
    
//    @Transactional
    public String deletePage(Car carItem) {
        car = em.find(Car.class, carItem.getId());
        id = car.getId();
        System.out.println("id: " + car.getId());
        return "delete";
    }
    
    @Transactional
    public String delete() {
        System.out.println("dd: " + id);
        car = em.find(Car.class, id);
        em.remove(car);
        return "index";
    }
}
