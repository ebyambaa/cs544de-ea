/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs544.cs.mum.edu.week10.entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostUpdate;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.TypedQuery;
import javax.persistence.Version;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Enkhee
 */
@Entity
@Table(name = "Week10_Car")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String make;
    private String model;
    @Column(name = "ManufacturingYear")
    private Integer year;
    private Integer miles;
    @Enumerated(EnumType.STRING)
    private Color color;

    @Column(name = "version_num")
    @Version
    private Integer version;

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Car() {
    }

    public String getMake() {
        return make;
    }

    public void setMake(String Make) {
        this.make = Make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String Model) {
        this.model = Model;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer Year) {
        this.year = Year;
    }

    public Integer getMiles() {
        return miles;
    }

    public void setMiles(Integer Miles) {
        this.miles = Miles;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color Color) {
        this.color = Color;
    }

    @Override
    public String toString() {
        return "Car{" + "id=" + id + ", make=" + make + ", model=" + model + ", year=" + year + ", miles=" + miles + ", color=" + color + ", version=" + version + '}';
    }

    
    public static List<Car> getAllCars(EntityManager em) {
        List<Car> cars = null;
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Car> cquery = builder.createQuery(Car.class);
        Root<Car> root = cquery.from(Car.class);
        cquery.select(root);
        TypedQuery<Car> query = em.createQuery(cquery);

        cars = query.getResultList();
        return cars;
    }
    
//    @PrePersist
    private void prePrintId(){
        printId("PrePersist");
    }

//    @PostPersist
    private void postPrintId(){
        printId("PostPersist");
    }
    
    @PreUpdate
    private void preUpdateId(){
        printId("PreUpdate");
    }
    
    @PostUpdate
    private void postUpdateId(){
        printId("PostUpdate");
    }
    
    private void printId(String str){
        System.out.println("callback ID from " + str + " : " + this.toString());
    }
 }
