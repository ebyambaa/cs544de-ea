/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo01.cs544.cs.mum.edu;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Enkhee
 */
@Entity
@Table(name = "CarTbl")
public class Car implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String Make;
    private String Model;
    @Column(name = "ManufacturingYear")
    private Integer Year;
    private Integer Miles;
    private Color Color;

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Car)) {
            return false;
        }
        Car other = (Car) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demo01.cs544.cs.mum.edu.Car[ id=" + id + " ]";
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the Make
     */
    public String getMake() {
        return Make;
    }

    /**
     * @param Make the Make to set
     */
    public void setMake(String Make) {
        this.Make = Make;
    }

    /**
     * @return the Model
     */
    public String getModel() {
        return Model;
    }

    /**
     * @param Model the Model to set
     */
    public void setModel(String Model) {
        this.Model = Model;
    }

    /**
     * @return the Year
     */
    public Integer getYear() {
        return Year;
    }

    /**
     * @param Year the Year to set
     */
    public void setYear(Integer Year) {
        this.Year = Year;
    }

    /**
     * @return the Miles
     */
    public Integer getMiles() {
        return Miles;
    }

    /**
     * @param Miles the Miles to set
     */
    public void setMiles(Integer Miles) {
        this.Miles = Miles;
    }

    /**
     * @return the Color
     */
    public Color getColor() {
        return Color;
    }

    /**
     * @param Color the Color to set
     */
    public void setColor(Color Color) {
        this.Color = Color;
    }

    public Car() {
    }

}
