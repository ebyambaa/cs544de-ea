package demo01.cs544.cs.mum.edu;


import demo01.cs544.cs.mum.edu.Car;
import demo01.cs544.cs.mum.edu.Color;
import demo01.cs544.cs.mum.edu.Student;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Enkhee
 */
public class MyFirstPersister {

    private EntityManagerFactory emf;
    private EntityManager em;

    public MyFirstPersister() {
        emf = Persistence.createEntityManagerFactory("Demo01_EntitiesPU");
        em = emf.createEntityManager();
    }

    public void start() {
        if (emf != null) {
            end();
        }
        emf = Persistence.createEntityManagerFactory("Demo01_EntitiesPU");
        em = emf.createEntityManager();
    }

    public void end() {
        if (emf != null) {
            em.close();
            emf.close();
            emf = null;
        }
    }

    public void save(Student student) {
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(student);
        tx.commit();
    }

    public void save(Car car) {
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(car);
        tx.commit();
    }

    public static void main(String[] args) {
        Student jack = new Student("Jack");
        jack.setCoursesTaken(5);
        jack.setGpa(3.3f);
        MyFirstPersister persister = new MyFirstPersister();
        persister.save(jack);
//        persister.end();
        
        Car car = new Car();
        car.setColor(Color.WHITE);
        car.setMake("Make");
        car.setMiles(100);
        car.setModel("model");
        car.setYear(1990);
        persister.save(car);
        persister.end();

    }
}
