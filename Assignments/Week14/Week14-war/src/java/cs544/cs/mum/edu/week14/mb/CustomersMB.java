/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs544.cs.mum.edu.week14.mb;

import cs544.cs.mum.edu.week14.entity.Customer;
import cs544.cs.mum.edu.week14.stateless.CustomerBean;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Enkhee
 */
@Named
@SessionScoped
public class CustomersMB implements Serializable {

    @EJB
    private CustomerBean customerBean;

    public CustomersMB() {
    }

//    @Interceptors(LoggingInterceptor.class)
    public List<Customer> getCustomers() {
        return customerBean.getCustomers();
    }

}
