/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs544.cs.mum.edu.week14.mb;

import cs544.cs.mum.edu.week14.entity.Customer;
import cs544.cs.mum.edu.week14.stateless.CustomerBean;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Enkhee
 */
@Named
@SessionScoped
public class CustomerMB implements Serializable {
    
    private Customer customer;
    @EJB
    private CustomerBean customerBean;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public CustomerMB() {
    }
    
    public String create(){
        customer = new Customer();
        return "customer";
    }
    
    public String save() {
        customerBean.updateCustomer(customer);
        return "index";
    }

    public String update(Customer customerIn) {
        customer = customerBean.getCustomer(customerIn.getId());
        return "customer";
    }

    public String delete(Customer customerIn) {
        customer = customerBean.getCustomer(customerIn.getId());
        customerBean.deleteCustomer(customerIn);
        return "index";
    }
}
