/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs544.cs.mum.edu.week14.stateless;

import cs544.cs.mum.edu.week14.entity.Customer;
import cs544.cs.mum.edu.week14.interceptor.LoggingInterceptor;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.interceptor.AroundInvoke;
import javax.interceptor.ExcludeClassInterceptors;
import javax.interceptor.Interceptors;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Enkhee
 */
@Stateless
@LocalBean
@Interceptors(LoggingInterceptor.class)
public class CustomerBean {

    @PersistenceContext(unitName = "Week14-ejbPU")
    private EntityManager em;
    private Logger logger = Logger.getLogger(CustomerBean.class.getName());

    public @NotNull
    Customer createCustomer(@NotNull Customer c) {
        em.persist(c);
        return c;
    }

    public @NotNull
    Customer updateCustomer(@NotNull Customer c) {
        return em.merge(c);
    }

    public Customer getCustomer(Long id) {
        return em.find(Customer.class, id);
    }

    public void deleteCustomer(@NotNull Customer c) {
        em.remove(em.merge(c));
    }

    @ExcludeClassInterceptors
    public List<Customer> getCustomers() {
        return Customer.getAllCustomers(em);
    }

    @AroundInvoke
    private Object logMethod(InvocationContext ic) throws Exception {
        logger.entering(ic.getTarget().toString(), ic.getMethod().getName());
        try {
            return ic.proceed();
        } finally {
            logger.exiting(ic.getTarget().toString(), ic.getMethod().getName());
        }
    }

}
