/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs544.cs.mum.edu.week6.dao;

import cs544.cs.mum.edu.week6.entity.Car;
import java.util.List;

/**
 *
 * @author Enkhee
 */
public interface CarDAO {
    
    List<Car> findAll();

    Car findById(int id);

    Integer create(Car car);

    void update(int id, Car car);

    void delete(int id);

}
