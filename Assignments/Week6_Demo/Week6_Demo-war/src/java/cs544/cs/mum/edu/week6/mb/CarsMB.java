/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs544.cs.mum.edu.week6.mb;

import cs544.cs.mum.edu.week6.entity.Car;
import cs544.cs.mum.edu.week6.service.CarService;
import cs544.cs.mum.edu.week6.service.CarServiceImpl;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Enkhee
 */
@Named(value = "carsMB")
@RequestScoped
public class CarsMB implements Serializable {

    @PersistenceContext(unitName = "Demo01_EntitiesPU")
    private EntityManager em;
    private List<Car> cars;
    private final CarService carService = new CarServiceImpl();

    public CarsMB() {
        cars = carService.getList();
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    public void deleteCar(Integer id) {
        carService.delete(id);
        cars = carService.getList();
    }

}
