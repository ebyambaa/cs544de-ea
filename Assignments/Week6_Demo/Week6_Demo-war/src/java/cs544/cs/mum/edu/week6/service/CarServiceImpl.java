/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs544.cs.mum.edu.week6.service;

import cs544.cs.mum.edu.week6.dao.CarDAO;
import cs544.cs.mum.edu.week6.dao.CarDAOImpl;
import cs544.cs.mum.edu.week6.entity.Car;
import java.util.List;

/**
 *
 * @author Enkhee
 */
public class CarServiceImpl implements CarService {

    CarDAO carDAO = new CarDAOImpl();

    public CarServiceImpl() {
    }

    @Override
    public List<Car> getList() {
        return carDAO.findAll();
    }

    @Override
    public Car get(Integer id) {
        return carDAO.findById(id);
    }

    @Override
    public Integer create(Car car) {
        return carDAO.create(car);
    }

    @Override
    public boolean update(Integer id, Car car) {
        try {
            carDAO.update(id, car);
            return true;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean delete(Integer id) {
        try {
            carDAO.delete(id);
            return true;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return false;
        }
    }

}
