/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs544.cs.mum.edu.week6.service;

import cs544.cs.mum.edu.week6.entity.Car;
import java.util.List;

/**
 *
 * @author Enkhee
 */
public interface CarService {
    List<Car> getList();

    Car get(Integer id);

    Integer create(Car car);

    boolean update(Integer id, Car car);

    boolean delete(Integer id);
    
}
