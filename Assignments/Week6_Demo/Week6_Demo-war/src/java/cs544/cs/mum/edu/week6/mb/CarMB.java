/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs544.cs.mum.edu.week6.mb;

import cs544.cs.mum.edu.week6.entity.Car;
import cs544.cs.mum.edu.week6.service.CarService;
import cs544.cs.mum.edu.week6.service.CarServiceImpl;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Enkhee
 */
@Named(value = "carMB")
@RequestScoped
public class CarMB implements Serializable {

    @PersistenceContext(unitName = "Demo01_EntitiesPU")
    private EntityManager em;
    private Car car;
    private final CarService carService = new CarServiceImpl();
    private String message = null;

    /**
     * Creates a new instance of CarMB
     */
    public CarMB() {
        car = new Car();
    }

    public String show(Integer id) {
        car = carService.get(id);
        return "Car";
    }

    public String save() {
        em.persist(car);
        
//        if (car.getId() == null || car.getId() == 0) {
//            int generatedId = carService.create(car);
//            message = "New car had been successfully created. GeneratedId - " + generatedId;
//            car.setId(generatedId);
//        } else {
//            boolean result = carService.update(car.getId(), car);
//            message = "Result of update action is " + result;
//        }
//        System.out.println(message);
        return "Cars";
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

}
