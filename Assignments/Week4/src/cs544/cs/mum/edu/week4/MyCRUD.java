/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs544.cs.mum.edu.week4;

/**
 *
 * @author Enkhee
 */
public class MyCRUD {

    public boolean deleteStudent(int studentId) {
        MyPersister p = new MyPersister("Week4PU");

        Student student = p.get(Student.class, studentId);
        if (student == null) {
            p.close();
            return false;
        } else {
            p.delete(student);
            p.close();
            return true;
        }
    }

    public static void main(String[] args) {
        MyCRUD m = new MyCRUD();
        System.out.println(m.deleteStudent(3));
//        System.out.println(m.deleteStudent(2));
    }
}
