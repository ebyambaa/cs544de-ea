/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs544.cs.mum.edu.week4;

/**
 *
 * @author Enkhee
 */
public class Main {

    public static void main(String[] args) {
        MyPersister p = new MyPersister("Week4PU");

        Student jack = new Student("jack", 3.4);
        jack.addNotebook(new Notebook(100));
        jack.addNotebook(new Notebook(120));
        jack.addNotebook(new Notebook(60));
        p.<Student>persist(jack);

        Student john = new Student("john", 3.2);
        john.addNotebook(new Notebook(200));
        john.addNotebook(new Notebook(50));
        p.<Student>persist(john);

        Student jill = new Student("jill", 3.6);
        jill.addNotebook(new Notebook(300));
        p.<Student>persist(jill);

        Teacher jim = new Teacher("jim", 45000);
        jim.setLaptop(new Laptop("dell"));
        p.<Teacher>persist(jim);

        Teacher jasmin = new Teacher("jasmin", 46000);
        jasmin.setLaptop(new Laptop("mac"));
        p.<Teacher>persist(jasmin);

        p.close();

    }

}
