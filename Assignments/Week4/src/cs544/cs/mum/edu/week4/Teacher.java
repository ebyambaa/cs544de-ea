/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs544.cs.mum.edu.week4;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

/**
 *
 * @author Enkhee
 */
@Entity
@DiscriminatorValue("teacher")
public class Teacher extends Person {
    private Integer salary;
    @OneToOne(fetch=FetchType.EAGER, cascade = CascadeType.ALL)
    private Laptop laptop;

    public Teacher() {
    }

    Teacher(String name, Integer s) {
        super(name);
        this.salary = s;
    }

    /**
     * @return the laptop
     */
    public Laptop getLaptop() {
        return laptop;
    }

    /**
     * @param laptop the laptop to set
     */
    public void setLaptop(Laptop laptop) {
        this.laptop = laptop;
    }

    /**
     * @return the salary
     */
    public Integer getSalary() {
        return salary;
    }

    /**
     * @param salary the salary to set
     */
    public void setSalary(Integer salary) {
        this.salary = salary;
    }
}
