/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs544.cs.mum.edu.week4;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Enkhee
 */
@Entity
public class Notebook {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    public Notebook() {
    }

    Notebook(Integer pages) {
        this.pages = pages;
    }

    /**
     * @return the pages
     */
    public Integer getCount() {
        return pages;
    }

    /**
     * @param pages the pages to set
     */
    public void setCount(Integer pages) {
        this.pages = pages;
    }
    private Integer pages;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
