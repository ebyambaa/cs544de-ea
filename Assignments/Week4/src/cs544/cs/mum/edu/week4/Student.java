/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs544.cs.mum.edu.week4;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

/**
 *
 * @author Enkhee
 */
@Entity
@DiscriminatorValue("student")
public class Student extends Person {

    private double gpa;
    @OneToMany(fetch=FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="student_fk")
    private List<Notebook> notebooks = new ArrayList<>();

    public Student() {
    }

    Student(String name, double g) {
        super(name);
        this.gpa = g;
    }

    /**
     * @return the notebooks
     */
    public List<Notebook> getNotebooks() {
        return notebooks;
    }

    /**
     * @param notebooks the notebooks to set
     */
    public void setNotebooks(List<Notebook> notebooks) {
        this.notebooks = notebooks;
    }

    public void addNotebook(Notebook notebook) {
        this.notebooks.add(notebook);
    }

    /**
     * @return the gpa
     */
    public double getGpa() {
        return gpa;
    }

    /**
     * @param gpa the gpa to set
     */
    public void setGpa(double gpa) {
        this.gpa = gpa;
    }

}
