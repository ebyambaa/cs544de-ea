/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs544.cs.mum.edu.week4;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author Enkhee
 */
public class MyPersister {

    private EntityManagerFactory emf;
    private EntityManager em;

    public MyPersister(String puName) {
        init(puName);
    }

    public void init(String puName) {
        emf = Persistence.createEntityManagerFactory(puName);
        em = emf.createEntityManager();
    }

    public void start(String puName) {
        if (emf == null) {
            init(puName);
        }
    }

    public void close() {
        if (emf != null) {
            em.close();
            emf.close();
            emf = null;
        }
    }

    public <T> void persist(T t) {
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(t);
        tx.commit();
    }

    public <T> T get(Class<T> t, Integer id) {
        return em.find(t, id);
    }

    public <T> void delete(T t) {
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(t);
        tx.commit();
    }
}
