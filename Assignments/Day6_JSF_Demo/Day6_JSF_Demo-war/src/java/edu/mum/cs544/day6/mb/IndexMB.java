/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs544.day6.mb;

import edu.mum.cs544.day6.business.DBFiller;
import edu.mum.cs544.day6.entity.Publisher;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 *
 * @author Enkhee
 */
@Named
@RequestScoped
public class IndexMB {

    @PersistenceContext(unitName = "Day6_JSF_Demo-warPU")
    private EntityManager em;
    private int pages;

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }
    DBFiller dbFiller = new DBFiller();
    /**
     * Creates a new instance of IndexMB
     */
    public IndexMB() {
    }

    @Transactional
    public String fillDB() {
        String page = null;
//        DBFiller dbFiller = new DBFiller();
        dbFiller.createAll(em);
        return page;
    }
    
    @Transactional
    public String clearDB() {
        String page = null;
//        DBFiller dbFiller = new DBFiller();
        dbFiller.clearAll(em);
        return page;
    }
    
    public List<Publisher> getPublishers(){
        return Publisher.getPublisherByBookPages(em, this.pages);
    }
}
