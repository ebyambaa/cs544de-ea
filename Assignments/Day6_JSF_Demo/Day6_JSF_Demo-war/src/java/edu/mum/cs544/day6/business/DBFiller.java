/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs544.day6.business;

import edu.mum.cs544.day6.entity.Address;
import edu.mum.cs544.day6.entity.Author;
import edu.mum.cs544.day6.entity.Book;
import edu.mum.cs544.day6.entity.Publisher;
import javax.persistence.EntityManager;

/**
 *
 * @author Enkhee
 */
public class DBFiller {
    public static final boolean createAll(EntityManager em){
        boolean didCreateAll = false;
        Address fairFieldCa = new Address("Ca", "FairField", "4th", "52557");
        Author antonio = new Author("Antonio", fairFieldCa, Author.GENDER.MALE);
        Book bookJEE = new Book("Beginning JEE", 673, antonio);
        Address ny = new Address("NY", "New York", "123456", "10013");
        Publisher apress = new Publisher("NY", ny);
        apress.addBook(bookJEE);
        Address fairFieldIowa = new Address("Iowa", "FairField", "4th", "52557");
        Author mrX = new Author("X", fairFieldIowa, Author.GENDER.MALE);
        Book bookEjb3 = new Book("EJB3", 400, mrX);
        apress.addBook(bookEjb3);
        em.persist(apress);

        Address fairFieldOh = new Address("Ohio", "FairField", "adscsd", "45157");
        Author jak = new Author("Jak", fairFieldOh, Author.GENDER.MALE);
        Book bookHow2Eat = new Book("How to eat", 120, jak);
        Address la = new Address("CA", "LA", "sdsgfswrg", "14512");
        
        Publisher firstPub = new Publisher("First Publisher", la);
        firstPub.addBook(bookHow2Eat);
        Address fairFieldIowa2 = new Address("Iowa", "FairField", "fsfeef", "52558");
        Author missY = new Author("X", fairFieldIowa2, Author.GENDER.FEMALE);
        Book cat = new Book("How love cat", 280, missY);
        firstPub.addBook(cat);
        em.persist(firstPub);

        didCreateAll = true;
        return didCreateAll;
    }
    
    public static final void clearAll(EntityManager em){
        for( Publisher publisher : Publisher.getAllPublishers(em)){
            em.remove(publisher);
        }
    }
}
