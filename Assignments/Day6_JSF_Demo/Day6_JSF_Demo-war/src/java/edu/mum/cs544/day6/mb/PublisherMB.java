/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs544.day6.mb;

import edu.mum.cs544.day6.business.DBFiller;
import edu.mum.cs544.day6.entity.Publisher;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Enkhee
 */
@Named
@RequestScoped
public class PublisherMB {
    @PersistenceContext(unitName = "Day6_JSF_Demo-warPU")
    private EntityManager em;
    DBFiller dbFiller = new DBFiller();

    /**
     * Creates a new instance of PublisherMB
     */
    public PublisherMB() {
    }
    public List<Publisher> getPublishers(){
     return Publisher.getAllPublishers(em);
    }
}
