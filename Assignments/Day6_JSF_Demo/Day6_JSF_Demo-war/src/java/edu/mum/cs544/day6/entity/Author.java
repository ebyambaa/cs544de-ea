/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs544.day6.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author Enkhee
 */
@Entity
public class Author {

    public enum GENDER {
        MALE, FEMALE
    };

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    @OneToMany(mappedBy = "author")
    private List<Book> books = new ArrayList<>();
    @OneToOne(cascade = CascadeType.ALL)
    private Address address;
    @OneToOne(cascade = CascadeType.ALL)
    private Publisher publisher;
    @Enumerated(EnumType.STRING)
    private GENDER gender;

    public Author() {
    }

    public Author(String name, Address address, GENDER gender) {
        this.name = name;
        this.address = address;
        this.gender = gender;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public String getName() {
        return name;
    }

    public List<Book> getBooks() {
        return books;
    }

    public Address getAddress() {
        return address;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void addBook(Book book) {
        this.books.add(book);
    }

    @Override
    public String toString() {
        return "Author{" + "id=" + id + ", name=" + name + ", books=" + books + ", address=" + address + ", publisher=" + publisher + ", gender=" + gender + '}';
    }
    
}
