/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mum.cs544.day6.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

/**
 *
 * @author Enkhee
 */
@Entity
public class Publisher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Book> publications = new ArrayList<>();
    @OneToOne(cascade = CascadeType.ALL)
    private Address address;

    public Publisher() {
    }

    public Publisher(String name, Address address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Book> getPublications() {
        return publications;
    }

    public void addBook(Book book) {
        this.publications.add(book);
    }

    public static List<Publisher> getAllPublishers(EntityManager em) {
        List<Publisher> publishers = null;
        String jpql = "Select p from Publisher p";
//        Query query = em.createQuery("Select p from Publisher p");
//        publishers = query.getResultList();

        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Publisher> cquery = builder.createQuery(Publisher.class);
        Root<Publisher> root = cquery.from(Publisher.class);
        cquery.select(root);
        TypedQuery<Publisher> query = em.createQuery(cquery);

//        TypedQuery<Publisher> query = em.createQuery(jpql, Publisher.class);
        publishers = query.getResultList();
        return publishers;
    }

    @Override
    public String toString() {
        return "Publisher{" + "id=" + id + ", name=" + name + ", publications=" + publications + ", address=" + address + '}';
    }

    public static List<Publisher> getPublisherByBookPages(EntityManager em, int pages) {
        List<Publisher> publishers = null;
//        String jpql = "Select distinct p from Publisher p INNER JOIN p.publications pub where pub.numberOfPages > :pages";
//        TypedQuery<Publisher> query = em.createQuery(jpql, Publisher.class);
//        query.setParameter("pages", pages);
        
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Publisher> cquery = builder.createQuery(Publisher.class);
        Root<Publisher> root = cquery.from(Publisher.class);
        Join<Publisher, Book> p = root.join(Publisher_.publications);
        cquery.where(builder.gt(p.get(Book_.numberOfPages), pages));
        cquery.select(root);
        cquery.distinct(true);
        TypedQuery<Publisher> query = em.createQuery(cquery);

        publishers = query.getResultList();
        return publishers;
    }
}
