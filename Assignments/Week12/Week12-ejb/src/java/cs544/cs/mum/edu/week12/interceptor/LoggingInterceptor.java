/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs544.cs.mum.edu.week12.interceptor;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

/**
 *
 * @author Enkhee
 */
@Interceptor
public class LoggingInterceptor implements Serializable {
    private Logger logger = Logger.getLogger(LoggingInterceptor.class.getName());

    @AroundInvoke
    public Object intercept(InvocationContext context) throws Exception {

        logger.info("before calling method :" 
                + context.getMethod().getName());

        Object[] params = context.getParameters();
        for (Object param : params) {
            if( param != null)
                logger.info("PARAM " + param.toString());
        }

        Object result = context.proceed();


        logger.info("after calling method :"
                + context.getMethod().getName());

        return result;

    }
//
//    @Inject
//    private Logger logger;
//
////    @AroundConstruct
////    private void init(InvocationContext ic) throws
////            Exception {
////        logger.fine("Entering constructor");
////        try {
////            ic.proceed();
////        } finally {
////            logger.fine("Exiting constructor");
////        }
////    }
//
//    @AroundInvoke
//    public Object logMethod(InvocationContext ic) throws Exception {
//        logger.entering(ic.getTarget().toString(), ic
//                .getMethod().getName());
//        try {
//            return ic.proceed();
//        } finally {
//            logger.exiting(ic.getTarget().toString(), ic
//                    .getMethod().getName());
//        }
//    }
//
}
